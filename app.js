function sum(str1, str2) {
  let result = "";
  let storageNumber = 0;
  let arrLength1 = str1.length;
  let arrLength2 = str2.length;

  for (let i = arrLength1 - 1, j = arrLength2 - 1; i >= 0 || j >= 0; i--, j--) {
    let a = i >= 0 ? Number(str1[i]) : 0;
    let b = j >= 0 ? Number(str2[j]) : 0;
    let s = a + b + storageNumber;
    result = (s % 10) + result;
    storageNumber = Math.floor(s / 10);
  }
  if (storageNumber > 0) {
    result = storageNumber + result;
  }
  console.log(`Result: ${result}`);
  return result;
}

console.log(sum("1234", "897"));
